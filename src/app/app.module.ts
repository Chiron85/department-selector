import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { DepartmentItemComponent } from "./components/department-item/department-item.component";
import { DepartmentListComponent } from "./components/department-list/department-list.component";
import { HttpClientModule } from "@angular/common/http";
import { DepartmentService } from "./service/department.service";

@NgModule({
  declarations: [
    AppComponent,
    DepartmentListComponent,
    DepartmentItemComponent
  ],
  imports: [BrowserModule, HttpClientModule],
  providers: [DepartmentService],
  bootstrap: [AppComponent]
})
export class AppModule {}
