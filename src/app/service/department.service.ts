import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Department } from "../models/department.model";

interface DepartmentRawJson {
  OID: number;
  Title: string;
  Color: string;
  DepartmentParent_OID: number;
}

@Injectable()
export class DepartmentService {
  constructor(private httpClient: HttpClient) {}

  baseUrl = environment.baseUrl;

  private chosenDep: Number[] = [118];

  public getDepartments(): Observable<any[]> {
    return this.httpClient
      .get<DepartmentRawJson[]>(this.baseUrl + "/assets/department-data.json")
      .pipe(
        map(data => {
          return this.getDataByOID(data, null);
        })
      );
  }

  private getDataByOID(data, OID) {
    const result = data.filter(d => d.DepartmentParent_OID === OID);
    return result.map(({ OID, Title, Color }) => ({
      OID,
      Title,
      Color,
      Children: this.getDataByOID(data, OID),
      showChildren: false,
      childSelected: 0,
      isSelected: false
    }));
  }

  public containsObject(obj: Department) {
    let i;
    let departments: Number[] = this.getChosenDepList();
    for (i = 0; i < departments.length; i++) {
      if (departments[i] === obj.OID) {
        return true;
      }
    }

    return false;
  }

  public getChosenDepList() {
    return this.chosenDep;
  }

  public addChosenDep(dep: Department) {
    dep.isSelected = true;
    this.chosenDep.push(dep.OID);
  }

  public removeChosenDep(dep: Department) {
    dep.isSelected = false;
    this.chosenDep.splice(this.chosenDep.indexOf(dep.OID), 1);
  }
}
