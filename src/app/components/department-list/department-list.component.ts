import { Component, OnInit } from "@angular/core";
import { Department } from "src/app/models/department.model";
import { DepartmentService } from "src/app/service/department.service";

@Component({
  selector: "app-department-list",
  templateUrl: "./department-list.component.html",
  styleUrls: ["./department-list.component.css"]
})
export class DepartmentListComponent implements OnInit {
  private departments: Department[] = [];

  constructor(private depService: DepartmentService) {}

  ngOnInit() {
    this.depService.getDepartments().subscribe(data => {
      this.departments = data;
    });
  }
}
