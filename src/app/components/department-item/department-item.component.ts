import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef
} from "@angular/core";
import { Department } from "src/app/models/department.model";
import { DepartmentService } from "src/app/service/department.service";

@Component({
  selector: "app-department-item",
  templateUrl: "./department-item.component.html",
  styleUrls: ["./department-item.component.css"]
})
export class DepartmentItemComponent implements OnInit {
  @Input() department: Department;
  @Output() depSelected = new EventEmitter<Department>();
  @Output() preSelected = new EventEmitter<Department>();

  constructor(
    private depService: DepartmentService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (this.depService.containsObject(this.department)) {
      this.department.isSelected = true;
      this.preSelected.emit(this.department);
    }
  }
  ngAfterViewChecked() {
    // Fixing error
    this.cdRef.detectChanges();
  }

  onClickDep(dep: Department) {
    if (dep.Children.length > 0) {
      dep.showChildren = !dep.showChildren;
    } else {
      if (this.depService.containsObject(dep)) {
        this.depService.removeChosenDep(dep);
        this.depSelected.emit(dep);
      } else {
        this.depService.addChosenDep(dep);
        this.depSelected.emit(dep);
      }
    }
  }

  handleClick(dep: Department) {
    if (this.depService.containsObject(dep)) {
      this.department.childSelected++;
      this.depSelected.emit(dep);
    } else {
      this.department.childSelected--;
      this.depSelected.emit(dep);
    }
  }

  handlePreselect(dep: Department) {
    this.department.showChildren = !this.department.showChildren;
    this.department.childSelected++;
    this.preSelected.emit(this.department);
  }

  getNumOfChildren(dep: Department) {
    return dep.Children.length;
  }
}
