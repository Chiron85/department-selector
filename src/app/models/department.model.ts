export class Department {
  constructor(
    public OID: number,
    public Title: string,
    public Color: string,
    public Children: Department[],
    public showChildren: boolean,
    public isSelected: boolean,
    public childSelected: number
  ) {}
}
